import { addDecorator, addParameters  } from '@storybook/react';
import { withConsole } from '@storybook/addon-console';

addDecorator((storyFn, context) => withConsole()(storyFn)(context));

addParameters({
    backgrounds: [
      { name: 'figma', value: '#E5E5E5', default: true },
      { name: 'black', value: '#000000' },
      { name: 'facebook', value: '#3b5998' },
    ],
  });