# Neitronik (Preview version)

This is not a complete version of the project and was created for code review purposes only. If you run `npm run dev`, you will get errors related to lack of files.

## Stack of technologies

- Next.js
- Storybook
- Sass
- Material-UI
- ESLint + Prettier

## Full project file structure

```
│   .eslintrc
│   .gitignore
│   .prettierrc
│   next-env.d.ts
│   next.config.js
│   package.json
│   README.md
│   tsconfig.json
│   yarn.lock
│
├───.storybook
│       main.js
│       manager.js
│       preview.js
│
├───pages
│   │   404.js
│   │   index.js
│   │   _app.js
│   │   _document.js
│   │
│   ├───cart
│   │       index.js
│   │
│   ├───pick_up_neitronik
│   │       index.js
│   │
│   └───preorder
│           index.js
│
├───public
│   │   .htaccess
│   │   favicon-16x16.png
│   │   favicon-32x32.png
│   │   favicon-48x48.png
│   │   favicon-64x64.png
│   │   letter.php
│   │   mail.php
│   │   robots.txt
│   │   send-mail.php
│   │
│   ├───assets
│   │   │   main.scss
│   │   │
│   │   ├───fonts
│   │   │   └───Roboto
│   │   │
│   │   ├───images
│   │   │
│   │   └───styles
│   │           _base.scss
│   │           _fonts.scss
│   │           _functions.scss
│   │           _variables.scss
│   │
│   └───lib
│       ├───Order
│       │
│       └───PHPMailer-master
│
└───src
    ├───actions
    │       home.js
    │       mapping.js
    │       pickUp.js
    │
    ├───components
    │   ├───Atoms
    │   │   ├───Button
    │   │   │       Button.stories.js
    │   │   │       index.js
    │   │   │       index.scss
    │   │   │
    │   │   ├───CartLink
    │   │   │       index.js
    │   │   │       index.scss
    │   │   │
    │   │   ├───Icon
    │   │   │       Icon.stories.js
    │   │   │       index.js
    │   │   │       index.scss
    │   │   │
    │   │   ├───InfoButton
    │   │   │       index.js
    │   │   │       index.scss
    │   │   │       InfoButton.stories.js
    │   │   │
    │   │   ├───LoadingIcon
    │   │   │       index.js
    │   │   │       index.scss
    │   │   │
    │   │   ├───RoundButton
    │   │   │       index.js
    │   │   │       index.scss
    │   │   │       RoundButton.stories.js
    │   │   │
    │   │   ├───RoundedInput
    │   │   │       index.js
    │   │   │       index.scss
    │   │   │       RoundedInput.stories.js
    │   │   │
    │   │   ├───RoundPanel
    │   │   │       index.js
    │   │   │       index.scss
    │   │   │       RoundPanel.stories.js
    │   │   │
    │   │   └───SliderDot
    │   │           ChangingProgressProvider.js
    │   │           index.js
    │   │           index.scss
    │   │           SliderDot.stories.js
    │   │
    │   ├───Containers
    │   │   └───ScreenContainer
    │   │           index.js
    │   │           index.scss
    │   │
    │   ├───Molecules
    │   │   ├───Counter
    │   │   │       Counter.stories.js
    │   │   │       index.js
    │   │   │       index.scss
    │   │   │
    │   │   ├───Header
    │   │   │       index.js
    │   │   │       index.scss
    │   │   │
    │   │   ├───PriceCounter
    │   │   │       index.js
    │   │   │       index.scss
    │   │   │       PriceCounter.stories.js
    │   │   │
    │   │   ├───Slider
    │   │   │       index.js
    │   │   │       index.scss
    │   │   │       Slider.stories.js
    │   │   │
    │   │   └───SliderDots
    │   │           index.js
    │   │           index.scss
    │   │           SliderDots.stories.js
    │   │
    │   ├───Organisms
    │   │   ├───CardProduct
    │   │   │       index.js
    │   │   │       style.scss
    │   │   │
    │   │   ├───DecoratedPanel
    │   │   │       DecoratedPanel.stories.js
    │   │   │       index.js
    │   │   │       index.scss
    │   │   │
    │   │   ├───Footer
    │   │   │       index.js
    │   │   │       index.scss
    │   │   │
    │   │   ├───MultiplePanel
    │   │   │       index.js
    │   │   │       index.scss
    │   │   │       MultiplePanel.stories.js
    │   │   │
    │   │   ├───SimplePanel
    │   │   │       index.js
    │   │   │       index.scss
    │   │   │       SimplePanel.stories.js
    │   │   │
    │   │   └───SliderPanel
    │   │           index.js
    │   │           index.scss
    │   │           SliderPanel.stories.js
    │   │
    │   └───Templates
    │       ├───CartTemplate
    │       │       index.js
    │       │       index.scss
    │       │
    │       ├───HomeTemplate
    │       │       index.js
    │       │       index.scss
    │       │
    │       ├───NotFoundTemplate
    │       │       index.scss
    │       │
    │       ├───PickUpTemplate
    │       │       index.js
    │       │       index.scss
    │       │
    │       └───PreorderTemplate
    │               index.js
    │               index.scss
    │
    ├───redux
    │   │   store.js
    │   │   types.js
    │   │
    │   └───reducers
    │           home.js
    │           index.js
    │           pickUp.js
    │
    └───tools
            functions.js
```
