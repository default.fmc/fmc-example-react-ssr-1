// PACKAGES
import React from "react";
import Head from "next/head";
import { connect } from "react-redux";
import SlickSlider from "react-slick";
import scrollToComponent from "react-scroll-to-component";
import YouTube from "react-youtube";
import Link from "next/link";
import { withRouter } from "next/router";
import { parseCookies } from "nookies";

// OTHER FILES
import { home } from "../src/actions/home";

// IMAGES
import bannerImage from "../public/assets/images/banner-decor.png";
import bannerImage_1920 from "../public/assets/images/banner-decor-1920.png";
import bannerImage_mobile from "../public/assets/images/banner-decor-mobile.png";
import LogoBigWebp from "../public/assets/images/neitronik-logo-big.webp";
import LogoBigPng from "../public/assets/images/ios-support/neitronik-logo-big.png";
import NeitronikChipWebp from "../public/assets/images/neitronik-chip.webp";
import NeitronikChipPng from "../public/assets/images/ios-support/neitronik-chip.png";
import NeitronikChip2Webp from "../public/assets/images/neitronik-chip2.webp";
import NeitronikChip2Png from "../public/assets/images/ios-support/neitronik-chip2.png";
import MonitorWebp from "../public/assets/images/monitor.webp";
import MonitorPng from "../public/assets/images/ios-support/monitor.png";
import FirstConnectorWebp from "../public/assets/images/first-connector.webp";
import FirstConnectorPng from "../public/assets/images/ios-support/first-connector.png";
import TiredGirlWebp from "../public/assets/images/tired-girl.webp";
import TiredGirlPng from "../public/assets/images/ios-support/tired-girl.png";
import SecondConnectorWebp from "../public/assets/images/second-connector.webp";
import SecondConnectorPng from "../public/assets/images/ios-support/second-connector.png";
import WorkingGirlWebp from "../public/assets/images/working-girl.webp";
import WorkingGirlPng from "../public/assets/images/ios-support/working-girl.png";
import ThirdConnectorWebp from "../public/assets/images/third-connector.webp";
import ThirdConnectorPng from "../public/assets/images/ios-support/third-connector.png";
import FourthConnectorWebp from "../public/assets/images/fourth-connector.webp";
import FourthConnectorPng from "../public/assets/images/ios-support/fourth-connector.png";
import FifthConnectorWebp from "../public/assets/images/fifth-connector.webp";
import FifthConnectorPng from "../public/assets/images/ios-support/fifth-connector.png";
import SixthConnectorWebp from "../public/assets/images/sixth-connector.webp";
import SixthConnectorPng from "../public/assets/images/ios-support/sixth-connector.png";
import LogoBlackWebp from "../public/assets/images/neitronik-logo-black.webp";
import LogoBlackPng from "../public/assets/images/ios-support/neitronik-logo-black.png";
import CloudIconSvg from "../public/assets/images/icons/cloud-icon.svg";
import ChipIconSvg from "../public/assets/images/icons/chip-icon.svg";
import RepairIconSvg from "../public/assets/images/icons/repair-icon.svg";

// COMPONENTS
import HomeTemplate from "../src/components/Templates/HomeTemplate";
import ScreenContainer from "../src/components/Containers/ScreenContainer";
import SliderDots from "../src/components/Molecules/SliderDots";
import SimplePanel from "../src/components/Organisms/SimplePanel";
import DecoratedPanel from "../src/components/Organisms/DecoratedPanel";
import MultiplePanel from "../src/components/Organisms/MultiplePanel";
import Footer from "../src/components/Organisms/Footer";
import SliderPanel from "../src/components/Organisms/SliderPanel";
import InfoButton from "../src/components/Atoms/InfoButton";
import PriceCounter from "../src/components/Molecules/PriceCounter";
import Header from "../src/components/Molecules/Header";

class HomePage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      mobileView: false,
      productsSum: 0,
      sliderHover: false,
      openSidebar: false,
      isPlaying: false,
    };
  }

  componentDidMount() {
    const dots = [];
    for (let i = 0; i < this.slider.innerSlider.state.slideCount; i++) {
      dots.push({
        id: i + 1,
        active: i === 0 ? true : false,
      });
    }
    this.setState({
      mobileView: window.innerWidth <= 799 ? true : false,
      productsSum: parseCookies().ProductsSumm
        ? JSON.parse(parseCookies().ProductsSumm)
        : 0,
    });
    home.editDotsData(dots);
    window.addEventListener("resize", this.handleResize);
    //this.slider.innerSlider.updateState();
  }

  componentDidUpdate(prevProps, prevState) {
    window.addEventListener("resize", this.handleResize);
  }

  handleResize = () => {
    if (window.innerWidth <= 799) {
      this.setState({ mobileView: true });
    } else {
      this.setState({ mobileView: false });
    }
  };

  changeSlide = (current, next) => {
    const newDots = this.props.home.dots.map((item) => {
      if (item.id === next + 1) {
        item.active = true;
      } else {
        item.active = false;
      }
      return item;
    });
    home.editDotsData(newDots);
  };

  onClick = (number) => {
    this.slider.slickGoTo(number);
    this.changeSlide(this.slider.innerSlider.state.currentSlide, number);
  };

  _onPlayVideo = (event) => {
    this.setState((state) => {
      return { isPlaying: !state.isPlaying };
    });
  };

  _onPauseVideo = (event) => {
    this.setState((state) => {
      return { isPlaying: !state.isPlaying };
    });
  };

  onMouseEnter = () => {
    this.setState({ sliderHover: true });
  };

  onMouseLeave = () => {
    this.setState({ sliderHover: false });
  };

  addToCart = (product) => {
    home.addToCart(product);
  };

  scrollToSection = (ref) => {
    scrollToComponent(ref, {
      align: "top",
      duration: 1000,
    });
  };

  toggleDrawer = () =>
    this.setState((state) => ({ openSidebar: !state.openSidebar }));

  render() {
    const settings = {
      className: "product-slider",
      dots: false,
      arrows: false,
      infinite: true,
      adaptiveHeight: true,
      centerMode: false,
      speed: 1000,
      slidesToShow: 1,
      slidesToScroll: 1,
      pauseOnHover: true,
      autoplay: true,
      autoplaySpeed: 7000,
      variableWidth: true,
      beforeChange: (current, next) => this.changeSlide(current, next),
      responsive: [
        {
          breakpoint: 1919,
          settings: {
            variableWidth: false,
          },
        },
      ],
    };

    const sliderPanelOptions = {
      className: "bottom-slider",
      dots: false,
      arrows: false,
      infinite: true,
      adaptiveHeight: false,
      speed: 1000,
      slidesToShow: 2,
      slidesToScroll: 1,
      pauseOnHover: false,
      autoplay: false,
      autoplaySpeed: 3000,
      variableWidth: false,
      responsive: [
        {
          breakpoint: 799,
          settings: {
            slidesToShow: 1,
          },
        },
      ],
    };
    return (
      <>
        <Head>
          <>
            <title>
              Neitronik - надёжная защита от электромагнитных излучений
            </title>
            <meta
              name="description"
              content="Продажа компактных чипов для защиты от электромагнитного излучения различных бытовых приборов."
            />
            <link
              rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css"
              integrity="sha512-wR4oNhLBHf7smjy0K4oqzdWumd+r5/+6QO/vDda76MW5iug4PT7v86FoEkySIJft3XA0Ae6axhIvHrqwm793Nw=="
              crossOrigin="anonymous"
            />
            <link
              rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.css"
              integrity="sha512-6lLUdeQ5uheMFbWm3CP271l14RsX1xtx+J5x2yeIDkkiBpeVTNhTqijME7GgRKKi6hCqovwCoBTlRBEC20M8Mg=="
              crossOrigin="anonymous"
            />
            <link
              rel="stylesheet"
              href="https://cdn.jsdelivr.net/npm/react-circular-progressbar@2.0.3/dist/styles.css"
              crossOrigin="anonymous"
            />
          </>
        </Head>
        <HomeTemplate>
          <Header
            productsSum={this.state.productsSum}
            navItems={[
              {
                id: 1,
                title: "Устройство",
                onClick: () => this.scrollToSection(this.screenTwo),
              },
              {
                id: 2,
                title: "О нас",
                onClick: () => this.scrollToSection(this.screenThree),
              },
              {
                id: 3,
                title: "Как установить",
                onClick: () => this.scrollToSection(this.screenFive),
              },
              {
                id: 4,
                title: "Исследования",
                onClick: () => this.scrollToSection(this.screenSix),
              },
            ]}
          />
          <main className="screens-container">
            <ScreenContainer classes={["screen-one"]} divider>
              <div className="screen-one__content-left">
                <div className="screen-one__logo">
                  <picture>
                    <source srcSet={LogoBigWebp} />
                    <img
                      decoding="async"
                      src={LogoBigPng}
                      alt="neitronik logo big"
                    />
                  </picture>
                </div>
                <div className="title-block-accent title-h1">
                  Надёжная защита от излучений 5G и 4G
                </div>
                <div className="simple-text screen-one__simple-text">
                  Нейтроник — наилучшая доступная технология защиты человека от
                  электромагнитного излучения{" "}
                  <span className="accent-text">
                    различных бытовых приборов
                  </span>
                  , а также мобильных сетей 5G и 4G.
                </div>
                {!this.state.mobileView && this.props.home.dots.length > 1 && (
                  <SliderDots
                    classes={["screen-one__dots"]}
                    timer={
                      settings.autoplay
                        ? settings.pauseOnHover && !this.state.sliderHover
                          ? settings.autoplaySpeed
                            ? settings.autoplaySpeed
                            : 3000
                          : null
                        : null
                    }
                    onClick={this.onClick}
                    dots={this.props.home.dots}
                  />
                )}
              </div>
              <div className="screen-one__content-right">
                <div className="screen-one__slider-shadow"></div>
                <div
                  className="screen-one__slider-wrap"
                  onMouseEnter={this.onMouseEnter}
                  onMouseLeave={this.onMouseLeave}
                >
                  <SlickSlider
                    ref={(slider) => (this.slider = slider)}
                    {...settings}
                  >
                    <SimplePanel classes={["screen-one__panel"]}>
                      <div className="banner screen-one__banner">
                        <div className="banner__decor">
                          <picture>
                            <source
                              srcSet={bannerImage}
                              media="(min-width: 1921px)"
                            />
                            <source
                              srcSet={bannerImage_1920}
                              media="(min-width: 800px)"
                            />
                            <source
                              srcSet={bannerImage_mobile}
                              media="(min-width: 320px)"
                            />
                            <img
                              decoding="async"
                              src={bannerImage}
                              alt="banner"
                            />
                          </picture>
                        </div>
                        <div className="banner__content">
                          <div className="banner__title">
                            КООПЕРИРУЙСЯ&nbsp;С
                            <br /> ДРУЗЬЯМИ И<br /> ПОЛУЧАЙ
                            <br /> СКИДКУ
                          </div>
                          <div className="banner__accent-text">
                            <div className="banner__small-number">5</div>
                            <div className="banner__middle-number">14</div>
                            <div className="banner__big-number">-20%</div>
                          </div>
                          <div className="banner__additional-info">
                            скидка дается за покупку наборов от 5 шт.
                          </div>
                        </div>
                      </div>
                    </SimplePanel>
                    <SimplePanel classes={["screen-one__panel"]}>
                      <div className="screen-one__content">
                        <div className="badge screen-one__badge">Original</div>
                        <div className="screen-one__left-part screen-one-left-part">
                          <picture>
                            <source
                              srcSet={NeitronikChipWebp}
                              type="image/webp"
                            />
                            <img
                              decoding="async"
                              src={NeitronikChipPng}
                              alt="neitronik chip"
                              className="screen-one__img"
                            />
                          </picture>
                        </div>
                        <div className="screen-one__right-part">
                          <div className="screen-one__title">
                            Neitronik
                            <br />
                            <span className="screen-one__sub-title">5GRS</span>
                          </div>
                          <div className="screen-one__text">
                            Нейтрализует вредные электромагнитные излучения
                            мобильных телефонов, смартфонов, радиотелефонов,
                            Wi-Fi-роутеров, СВЧ-печей (на частотах свыше 450
                            МГц)
                          </div>
                          <PriceCounter
                            id={1}
                            onClickButton={() =>
                              this.props.router.push("/cart")
                            }
                            productName={"Neitronik 5GRS"}
                            productPrice={400}
                            returnProduct={this.addToCart}
                            classes={["screen-one__price-counter"]}
                          />
                        </div>
                      </div>
                    </SimplePanel>
                    <SimplePanel classes={["screen-one__panel"]}>
                      <div className="screen-one__content">
                        <div className="badge screen-one__badge">Original</div>
                        <div className="screen-one__left-part screen-one-left-part">
                          <picture>
                            <source srcSet={MonitorWebp} type="image/webp" />
                            <img
                              decoding="async"
                              src={MonitorPng}
                              alt="neitronik monitor"
                              className="screen-one__img"
                            />
                          </picture>
                        </div>
                        <div className="screen-one__right-part">
                          <div className="screen-one__title">
                            Neitronik
                            <br />
                            <span className="screen-one__sub-title">
                              MG-04M
                            </span>
                          </div>
                          <div className="screen-one__text">
                            Нейтрализует излучения с частотой ниже 450 МГц.
                            Устанавливается на телевизоры, мониторы ПК,
                            ноутбуки, планшеты, электронные книги, а также
                            плазменные, ЖК и ЭЛТ дисплеи разного назначения.
                          </div>
                          <PriceCounter
                            id={2}
                            onClickButton={() =>
                              this.props.router.push("/cart")
                            }
                            productName={"Neitronik MG-04M"}
                            productPrice={364}
                            returnProduct={this.addToCart}
                            classes={["screen-one__price-counter"]}
                          />
                        </div>
                      </div>
                    </SimplePanel>
                  </SlickSlider>
                </div>
              </div>
              {this.state.mobileView && this.props.home.dots.length > 1 && (
                <SliderDots
                  classes={["screen-one__dots"]}
                  timer={
                    settings.autoplay
                      ? settings.autoplaySpeed
                        ? settings.autoplaySpeed
                        : 3000
                      : null
                  }
                  onClick={this.onClick}
                  dots={this.props.home.dots}
                />
              )}
              <div className="connect-image screen-one__connect-image">
                <picture>
                  <source
                    media="(min-width: 800px)"
                    srcSet={FirstConnectorWebp}
                    type="image/webp"
                  />
                  <source
                    media="(max-width: 799px)"
                    srcSet="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAApJREFUCNdjYAAAAAIAAeIhvDMAAAAASUVORK5CYII="
                  />
                  <img
                    decoding="async"
                    src={FirstConnectorPng}
                    alt="connect line"
                  />
                </picture>
              </div>
            </ScreenContainer>
            <ScreenContainer
              classes={["screen-two"]}
              ref={(section) => {
                this.screenTwo = section;
              }}
              divider
            >
              <div id="screen-two" className="screen-two__panel">
                <DecoratedPanel
                  classes={["screen-two__decorated-panel"]}
                  buttonText="ПОДОБРАТЬ"
                  onClick={() => this.props.router.push("/pick_up_neitronik")}
                  iconUrl={CloudIconSvg}
                >
                  <div className="decorated-panel__content">
                    <div className="decorated-panel__title">Защити себя</div>
                    <div className="decorated-panel__text">
                      <p>
                        Нейтроник — уникальное средство защиты от
                        электромагнитных излучений, по эффективности не имеющее
                        аналогов, а также очень прост в использовании.
                        Устройство устраняет вредное влияние излучений на
                        организм, при этом первоначальные характеристики бытовой
                        техники не ухудшаются.
                      </p>
                      <p>
                        Детальную информацию об исследованиях, сертификатах и
                        отзывах можно найти на официальном сайте -{" "}
                        <a
                          className="external-link"
                          href="http://neitronik.ru"
                          target="_blank"
                          rel="nofollow noopener noreferrer"
                        >
                          neitronik.ru
                        </a>
                      </p>
                      <p>
                        <span className="accent-text">
                          Мы официальный представитель в Украине.
                        </span>{" "}
                        У нас только оригинальные изделия. Ссылку на наш ресурс
                        вы можете найти на официальном сайте Нейтроника.
                      </p>
                    </div>
                  </div>
                </DecoratedPanel>
              </div>
              <div className="screen-two__image">
                <picture>
                  <source
                    media="(min-width: 800px)"
                    srcSet={TiredGirlWebp}
                    type="image/webp"
                  />
                  <source
                    media="(max-width: 799px)"
                    srcSet="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAApJREFUCNdjYAAAAAIAAeIhvDMAAAAASUVORK5CYII="
                  />
                  <img decoding="async" src={TiredGirlPng} alt="tired girl" />
                </picture>
              </div>
              <div className="connect-image screen-two__connect-image">
                <picture>
                  <source
                    media="(min-width: 800px)"
                    srcSet={SecondConnectorWebp}
                    type="image/webp"
                  />
                  <source
                    media="(max-width: 799px)"
                    srcSet="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAApJREFUCNdjYAAAAAIAAeIhvDMAAAAASUVORK5CYII="
                  />
                  <img
                    decoding="async"
                    src={SecondConnectorPng}
                    alt="connect line"
                  />
                </picture>
              </div>
            </ScreenContainer>
            <ScreenContainer
              classes={["screen-three"]}
              ref={(section) => {
                this.screenThree = section;
              }}
              divider
            >
              <div className="screen-three__image">
                <picture>
                  <source
                    media="(min-width: 800px)"
                    srcSet={WorkingGirlWebp}
                    type="image/webp"
                  />
                  <source
                    media="(max-width: 799px)"
                    srcSet="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAApJREFUCNdjYAAAAAIAAeIhvDMAAAAASUVORK5CYII="
                  />
                  <img decoding="async" src={WorkingGirlPng} alt="tired girl" />
                </picture>
              </div>
              <div className="screen-three__panel">
                <DecoratedPanel
                  classes={["screen-three__decorated-panel"]}
                  buttonText="ПОДОБРАТЬ"
                  onClick={() => this.props.router.push("/pick_up_neitronik")}
                  iconUrl={ChipIconSvg}
                >
                  <div className="decorated-panel__content">
                    <div className="decorated-panel__title">
                      Только оригинальные изделия
                    </div>
                    <div className="decorated-panel__text">
                      <p>
                        Мы первый официальный представитель Нейтроников в
                        Украине! Мы рады предоставить вам оригинальные изделия.
                        Ссылку на наш ресурс вы можете найти на официальном
                        сайте{" "}
                        <a
                          className="external-link"
                          href="https://neitronik.com/partners/"
                          target="_blank"
                          rel="nofollow noopener noreferrer"
                        >
                          neitronik.ru
                        </a>
                      </p>
                      <p>
                        Действие прибора основано на эксклюзивной
                        запатентованной технологии. Ноу-хау заключается в
                        уникальной форме объемной фигуры, которая проецируется
                        на плоскость, образуя защитную матрицу. Таким образом,
                        получается пассивная антенна, способная преобразовать
                        опасные для человека электромагнитные волны. Пассивное
                        воздействие отлично справляется со всеми излучениями
                        бытовых приборов, при этом создавая дополнительное
                        защитное поле, благотворно влияющее на человека.
                      </p>
                      <p>
                        Остерегайтесь подделок - заказывайте только у
                        официальных представителей Нейтроник.
                      </p>
                    </div>
                  </div>
                </DecoratedPanel>
              </div>
              <div className="connect-image screen-three__connect-image">
                <picture>
                  <source
                    media="(min-width: 800px)"
                    srcSet={ThirdConnectorWebp}
                    type="image/webp"
                  />
                  <source
                    media="(max-width: 799px)"
                    srcSet="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAApJREFUCNdjYAAAAAIAAeIhvDMAAAAASUVORK5CYII="
                  />
                  <img
                    decoding="async"
                    src={ThirdConnectorPng}
                    alt="connect line"
                  />
                </picture>
              </div>
            </ScreenContainer>
            <ScreenContainer
              ref={(section) => {
                this.screenFour = section;
              }}
              classes={["screen-four"]}
              divider
            >
              <div className="screen-four__panel">
                <MultiplePanel
                  classes={["screen-four__multiple-panel"]}
                  headerContent="При заказе Вы получаете наростающую скидку на весь заказ"
                >
                  <div className="multiple-panel__item">
                    <div className="panel-content">
                      <div className="badge panel-content__badge">
                        <div>Original</div>
                      </div>
                      <div className="panel-content__top">
                        <div className="panel-content__image">
                          <picture>
                            <source
                              srcSet={NeitronikChipWebp}
                              type="image/webp"
                            />
                            <img
                              decoding="async"
                              src={NeitronikChipPng}
                              alt="neitronik chip"
                            />
                          </picture>
                        </div>
                        <div className="panel-content__description">
                          <div className="panel-content__title">
                            Neitronik
                            <br />
                            <span className="panel-content__sub-title">
                              5GRS
                            </span>
                          </div>
                          <div className="panel-content__text">
                            Нейтрализует вредные электромагнитные излучения
                            мобильных телефонов, смартфонов, радиотелефонов,
                            Wi-Fi-роутеров, СВЧ-печей (на частотах свыше 450
                            МГц)
                          </div>
                        </div>
                      </div>
                      <PriceCounter
                        id={3}
                        onClickButton={() => this.props.router.push("/cart")}
                        productName={"Neitronik 5GRS"}
                        productPrice={400}
                        returnProduct={this.addToCart}
                        classes={["panel-content__price-counter"]}
                      />
                    </div>
                  </div>
                  <div className="multiple-panel__item multiple-panel__item_last">
                    <div className="panel-content">
                      <div className="badge panel-content__badge">Original</div>
                      <div className="panel-content__top">
                        <div className="panel-content__image">
                          <picture>
                            <source
                              srcSet={NeitronikChip2Webp}
                              type="image/webp"
                            />
                            <img
                              decoding="async"
                              src={NeitronikChip2Png}
                              alt="neitronik chip"
                            />
                          </picture>
                        </div>
                        <div className="panel-content__description">
                          <div className="panel-content__title">
                            Neitronik
                            <br />
                            <span className="panel-content__sub-title">
                              MG-04M
                            </span>
                          </div>
                          <div className="panel-content__text">
                            Нейтрализует излучения с частотой ниже 450 МГц.
                            Устанавливается на телевизоры, мониторы ПК,
                            ноутбуки, планшеты, электронные книги, а также
                            плазменные, ЖК и ЭЛТ дисплеи разного назначения.
                          </div>
                        </div>
                      </div>
                      <PriceCounter
                        id={4}
                        onClickButton={() => this.props.router.push("/cart")}
                        productName={"Neitronik MG-04M"}
                        productPrice={364}
                        returnProduct={this.addToCart}
                        classes={["panel-content__price-counter"]}
                      />
                    </div>
                  </div>
                </MultiplePanel>
              </div>
              <div className="connect-image screen-four__connect-image">
                <picture>
                  <source
                    media="(min-width: 800px)"
                    srcSet={FourthConnectorWebp}
                    type="image/webp"
                  />
                  <source
                    media="(max-width: 799px)"
                    srcSet="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAApJREFUCNdjYAAAAAIAAeIhvDMAAAAASUVORK5CYII="
                  />
                  <img
                    decoding="async"
                    src={FourthConnectorPng}
                    alt="connect line"
                  />
                </picture>
              </div>
            </ScreenContainer>
            <ScreenContainer
              classes={["screen-five"]}
              ref={(section) => {
                this.screenFive = section;
              }}
              divider
            >
              <div className="screen-five__panel">
                <DecoratedPanel
                  classes={["screen-five__decorated-panel"]}
                  buttonText="ПОДОБРАТЬ"
                  onClick={() => this.props.router.push("/pick_up_neitronik")}
                  iconUrl={RepairIconSvg}
                  isPlayingVideo={this.state.isPlaying}
                  mobileView={this.state.mobileView}
                >
                  <div className="decorated-panel__content">
                    <div className="video-placeholder">
                      <YouTube
                        videoId="1lB58NUTNlk"
                        className="video-container"
                        onPlay={this._onPlayVideo}
                        onPause={this._onPauseVideo}
                      />
                    </div>
                    {this.state.mobileView ? (
                      <div className="screen-five__description">
                        <div className="screen-five__title">
                          Как установить нейтроник?
                        </div>
                        <div className="screen-five__text">
                          <p>
                            Для использования нейтроника не нужно иметь
                            каких-либо специальных навыков, с ним справится
                            каждый. Действие прибора начинается сразу же после
                            его установки, а максимальный эффект достигается
                            через несколько дней. Соблюдайте простые правила
                            эксплуатации и его защитный эффект будет сохраняться
                            неограниченное время, при этом качество не будет
                            ухудшаться.
                          </p>
                          <p>
                            Закрепив Нейтроник на устройство, не подвергайте его
                            физическому воздействию и отрыванию. При нарушении
                            целостности голограммы возможно ухудшении
                            качественных характеристик или полный выход из
                            строя.
                          </p>
                          <p>
                            Не переносите Нейтроник с одного излучающего
                            устройства на другое. Частая смена положения не дает
                            возможности накопить максимальную эффективность, для
                            этого нужно время.
                          </p>
                        </div>
                      </div>
                    ) : (
                      ""
                    )}
                  </div>
                </DecoratedPanel>
              </div>
              {!this.state.mobileView ? (
                <div className="screen-five__description">
                  <div className="screen-five__title">
                    Как установить нейтроник?
                  </div>
                  <div className="screen-five__text">
                    <p>
                      Для использования нейтроника не нужно иметь каких-либо
                      специальных навыков, с ним справится каждый. Действие
                      прибора начинается сразу же после его установки, а
                      максимальный эффект достигается через несколько дней.
                      Соблюдайте простые правила эксплуатации и его защитный
                      эффект будет сохраняться неограниченное время, при этом
                      качество не будет ухудшаться.
                    </p>
                    <p>
                      Закрепив Нейтроник на устройство, не подвергайте его
                      физическому воздействию и отрыванию. При нарушении
                      целостности голограммы возможно ухудшении качественных
                      характеристик или полный выход из строя.
                    </p>
                    <p>
                      Не переносите Нейтроник с одного излучающего устройства на
                      другое. Частая смена положения не дает возможности
                      накопить максимальную эффективность, для этого нужно
                      время.
                    </p>
                  </div>
                </div>
              ) : (
                ""
              )}
              <div className="connect-image screen-five__connect-image">
                <picture>
                  <source
                    media="(min-width: 800px)"
                    srcSet={FifthConnectorWebp}
                    type="image/webp"
                  />
                  <source
                    media="(max-width: 799px)"
                    srcSet="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAApJREFUCNdjYAAAAAIAAeIhvDMAAAAASUVORK5CYII="
                  />
                  <img
                    decoding="async"
                    src={FifthConnectorPng}
                    alt="connect line"
                  />
                </picture>
              </div>
            </ScreenContainer>
            <ScreenContainer
              classes={["screen-six"]}
              ref={(section) => {
                this.screenSix = section;
              }}
            >
              <SliderPanel
                classes={["screen-six__slider-panel"]}
                headerContent="Исследования"
                sliderOptions={sliderPanelOptions}
                enableDots
              >
                <div className="slider-panel__item">
                  <div className="slider-panel__content">
                    <div className="slider-panel__inner">
                      <div className="slider-panel__top">
                        <div className="slider-panel__title">
                          Зарубежные испытания
                        </div>
                        <div className="slider-panel__text">
                          Нейтроник был признан не только в России, но и в
                          других странах. Испытания нашего устройства проходили
                          в странах Европы, которые также подтвердили его
                          эффективность.
                        </div>
                      </div>
                      <div className="slider-panel__bottom">
                        <a
                          className="slider-panel__link"
                          href="http://neitronik.ru/test-results/article_post/abroad-tests"
                          target="_blank"
                          rel="nofollow noopener noreferrer"
                        >
                          <InfoButton>Читать</InfoButton>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="slider-panel__item">
                  <div className="slider-panel__content">
                    <div className="slider-panel__inner">
                      <div className="slider-panel__top">
                        <div className="slider-panel__title">
                          Испытания по методу функционального гемосканирования
                        </div>
                        <div className="slider-panel__text">
                          Когда человек разговаривает по телефону, состояние его
                          крови и её работа существенно меняются. Эритроциты,
                          отвечающие за доставку кислорода к органам и тканям,
                          слипаются друг с другом и замедляют свою
                          работоспособность. Нейтроник решает эту проблему.
                        </div>
                      </div>
                      <div className="slider-panel__bottom">
                        <a
                          className="slider-panel__link"
                          href="http://neitronik.ru/test-results/article_post/blood-tests"
                          target="_blank"
                          rel="nofollow noopener noreferrer"
                        >
                          <InfoButton>Читать</InfoButton>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="slider-panel__item">
                  <div className="slider-panel__content">
                    <div className="slider-panel__inner">
                      <div className="slider-panel__top">
                        <div className="slider-panel__title">
                          Прошел проверку по методу доктора Р. Фолля
                        </div>
                        <div className="slider-panel__text">
                          Устройство Нейтроника прошло эксресс-диагностику по
                          методу доктора Р. Фолля. При использовании мобильного
                          телефона, телевизора, компьютера, организм человека
                          находился в нормальном состоянии. Нейтроник
                          существенно снизил нагрузку излучения.
                        </div>
                      </div>
                      <div className="slider-panel__bottom">
                        <a
                          className="slider-panel__link"
                          href="http://neitronik.ru/test-results/article_post/foll-tests"
                          target="_blank"
                          rel="nofollow noopener noreferrer"
                        >
                          <InfoButton>Читать</InfoButton>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="slider-panel__item">
                  <div className="slider-panel__content">
                    <div className="slider-panel__inner">
                      <div className="slider-panel__top">
                        <div className="slider-panel__title">
                          Проведенные испытания, которые доказывают
                          эффективность устройства
                        </div>
                        <div className="slider-panel__text">
                          Результаты биологических и технических испытаний
                          подтвердили эффективность Нейтроника
                        </div>
                      </div>
                      <div className="slider-panel__bottom">
                        <a
                          className="slider-panel__link"
                          href="http://neitronik.ru/test-results/article_post/test-types"
                          target="_blank"
                          rel="nofollow noopener noreferrer"
                        >
                          <InfoButton>Читать</InfoButton>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="slider-panel__item">
                  <div className="slider-panel__content">
                    <div className="slider-panel__inner">
                      <div className="slider-panel__top">
                        <div className="slider-panel__title">
                          Испытания на микроорганизмах
                        </div>
                        <div className="slider-panel__text">
                          В 2014 г. профессор Симаков провел ряд экспериментов
                          на наличие и силу воздействия излучения компьютеров и
                          мобильных устройств относительно микроорганизмов. А
                          именно они лучший показатель того, какую реакцию
                          вызывает электромагнитное излучение. В эти
                          исследования было включено защитное устройство
                          "Нейтроник", где в очередной раз показало свою
                          эффективность.
                        </div>
                      </div>
                      <div className="slider-panel__bottom">
                        <a
                          className="slider-panel__link"
                          href="http://neitronik.ru/test-results/article_post/bacters_test"
                          target="_blank"
                          rel="nofollow noopener noreferrer"
                        >
                          <InfoButton>Читать</InfoButton>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="slider-panel__item">
                  <div className="slider-panel__content">
                    <div className="slider-panel__inner">
                      <div className="slider-panel__top">
                        <div className="slider-panel__title">
                          Радиофизические испытания
                        </div>
                        <div className="slider-panel__text">
                          Эффективность Нейтроника относительно защиты человека
                          от теплового воздействия доказана, как одно из лучших
                          поглощающих устройств
                        </div>
                      </div>
                      <div className="slider-panel__bottom">
                        <a
                          className="slider-panel__link"
                          href="http://neitronik.ru/test-results/article_post/radiofizicheskiye-ispytaniya"
                          target="_blank"
                          rel="nofollow noopener noreferrer"
                        >
                          <InfoButton>Читать</InfoButton>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="slider-panel__item">
                  <div className="slider-panel__content">
                    <div className="slider-panel__inner">
                      <div className="slider-panel__top">
                        <div className="slider-panel__title">
                          Проверка камерой-ГРВ
                        </div>
                        <div className="slider-panel__text">
                          Физическое и анатомическое состояние организма
                          человека наиболее эффективно проверять методом
                          газоразрядной визуализации. Это метод имени профессора
                          Короткова, работает по эффекту Кирлиан
                        </div>
                      </div>
                      <div className="slider-panel__bottom">
                        <a
                          className="slider-panel__link"
                          href="http://neitronik.ru/test-results/article_post/proverka-s-pomoshchyu-grv-kamery"
                          target="_blank"
                          rel="nofollow noopener noreferrer"
                        >
                          <InfoButton>Читать</InfoButton>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </SliderPanel>
              <div className="connect-image screen-six__connect-image">
                <picture>
                  <source
                    media="(min-width: 800px)"
                    srcSet={SixthConnectorWebp}
                    type="image/webp"
                  />
                  <source
                    media="(max-width: 799px)"
                    srcSet="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAApJREFUCNdjYAAAAAIAAeIhvDMAAAAASUVORK5CYII="
                  />
                  <img
                    decoding="async"
                    src={SixthConnectorPng}
                    alt="connect line"
                  />
                </picture>
              </div>
            </ScreenContainer>
          </main>
          <Footer>
            <div className="footer__left">
              <Link href="/">
                <a className="footer__logo">
                  <picture>
                    <source srcSet={LogoBlackWebp} type="image/webp" />
                    <img decoding="async" src={LogoBlackPng} alt="Logo" />
                  </picture>
                </a>
              </Link>
            </div>
            <div className="footer__right">
              <nav className="footer__nav">
                <span
                  onClick={() => this.scrollToSection(this.screenTwo)}
                  className="footer__nav-link"
                >
                  Устройство
                </span>
                <span
                  onClick={() => this.scrollToSection(this.screenThree)}
                  className="footer__nav-link"
                >
                  О нас
                </span>
                <span
                  onClick={() => this.scrollToSection(this.screenFive)}
                  className="footer__nav-link"
                >
                  Как установить
                </span>
                <span
                  onClick={() => this.scrollToSection(this.screenSix)}
                  className="footer__nav-link"
                >
                  Исследования
                </span>
              </nav>
            </div>
          </Footer>
        </HomeTemplate>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  home: state.home,
});

export default connect(mapStateToProps)(withRouter(HomePage));
