// PACKAGES
import App from "next/app";
import React from "react";
import { Provider } from "react-redux";
import { createWrapper } from "next-redux-wrapper";
import CssBaseline from "@material-ui/core/CssBaseline";
import store from "../src/redux/store";
import ReactGA from "react-ga";

// STYLES
import "../public/assets/main.scss";
import Head from "next/head";

class MyApp extends App {
  componentDidMount() {
    ReactGA.initialize("XX-XXXXXXXXX-X", { debug: false });
    ReactGA.set({ page: this.props.router.pathname });
    ReactGA.pageview(this.props.router.pathname);
    this.unlisten = this.props.router.events.on(
      "routeChangeComplete",
      (url) => {
        window.scrollTo(0, 0);
        ReactGA.set({ page: url });
        ReactGA.pageview(url);
      }
    );
  }

  render() {
    const { Component, pageProps } = this.props;

    return (
      <>
        <Head>
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <link
            rel="icon"
            type="image/png"
            href="/favicon-16x16.png"
            sizes="16x16"
          />
          <link
            rel="icon"
            type="image/png"
            href="/favicon-32x32.png"
            sizes="32x32"
          />
          <link
            rel="icon"
            type="image/png"
            href="/favicon-48x48.png"
            sizes="48x48"
          />
          <link
            rel="icon"
            type="image/png"
            href="/favicon-64x64.png"
            sizes="64x64"
          />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <meta name="theme-color" content="#4b9a1f" />
          <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.css"
            integrity="sha512-IJEbgDEF7OeKJRa0MY2PApnyJHRIsgzCveek4ec8VWQ+7KG3ZSKVNYa5xP/Gh0hVP0Mwb+gBsk+GwR3JQGhQNg=="
            crossOrigin="anonymous"
          />
        </Head>
        <Provider store={store}>
          <CssBaseline />
          <Component {...pageProps} />
        </Provider>
      </>
    );
  }
}

const makeStore = () => store;
const wrapper = createWrapper(makeStore);

export default wrapper.withRedux(MyApp);
