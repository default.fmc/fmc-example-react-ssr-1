import React from "react";
import "../src/components/Templates/NotFoundTemplate/index.scss";
import Head from "next/head";

export class NotFoundPage extends React.Component {
  render() {
    return (
      <>
        <Head>
          <title>404: Page not found</title>
        </Head>
        <div className="container">
          <div className="not-found">
            <h1 className="not-found__title">404</h1>
            <div className="not-found__text">PAGE NOT FOUND</div>
          </div>
        </div>
      </>
    );
  }
}

export default NotFoundPage;
