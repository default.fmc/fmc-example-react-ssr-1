const withSass = require("@zeit/next-sass");
const withFonts = require("nextjs-fonts");
const withPlugins = require("next-compose-plugins");
const withImages = require("next-images");

module.exports = withPlugins([[withSass], [withFonts], [withImages]]);
