import React from "react";
import Icon from ".";
import { withKnobs } from "@storybook/addon-knobs";
import IconCloud from "../../../../public/assets/images/icons/cloud-icon.svg";

export default {
  title: "Atoms|Icon",
  component: Icon,
  decorators: [withKnobs],
};

export const Default = () => <Icon url={IconCloud} />;
