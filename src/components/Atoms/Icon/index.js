import React, { Component } from "react";
import PropTypes from "prop-types";
import "./index.scss";

export class Icon extends Component {
  static propTypes = {
    classes: PropTypes.arrayOf(PropTypes.string),
    url: PropTypes.string,
  };

  render() {
    return (
      <img
        src={this.props.url}
        alt="custom icon"
        className={`custom-icon ${
          this.props.classes ? this.props.classes.join(" ") : ""
        }`}
      />
    );
  }
}

export default Icon;
