import React, { Component } from "react";
import Link from "next/link";
import PropTypes from "prop-types";
import "./index.scss";

export class CartLink extends Component {
  onClick = (e) => {
    if (!this.props.cartSumm) {
      e.preventDefault();
    }
  };

  render() {
    return (
      <Link href="/cart">
        <a onClick={(e) => this.onClick(e)} className="cart-link">
          {this.props.cartSumm > 0 ? (
            <div className="cart-link__icon cart-link_full"></div>
          ) : (
            <div className="cart-link__icon cart-link_empty"></div>
          )}
          {this.props.cartSumm > 0 ? (
            <div className="cart-link__text">
              {this.props.cartSumm} {"грн."}
            </div>
          ) : (
            <div className="cart-link__text">Корзина</div>
          )}
        </a>
      </Link>
    );
  }
}

CartLink.propTypes = {
  cartSumm: PropTypes.number,
};

export default CartLink;
