import React from "react";
import RoundedInput from ".";
import { action } from "@storybook/addon-actions";
import { withKnobs, text, boolean } from "@storybook/addon-knobs";

export default {
  title: "Atoms|RoundedInput",
  component: RoundedInput,
  decorators: [withKnobs],
};

export const Default = () => (
  <RoundedInput
    value={text("Input text", "1")}
    disabled={boolean("Disabled", false)}
    onChange={action("change")}
  />
);
