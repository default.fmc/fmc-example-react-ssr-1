import React, { Component } from "react";
import PropTypes from "prop-types";
import "./index.scss";

export class RoundedInput extends Component {
  static propTypes = {
    id: PropTypes.number,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    disabled: PropTypes.bool,
    onChange: PropTypes.func,
  };

  componentDidMount() {
    this.uniqueId = this.getRandomInt(10000);
  }

  getRandomInt = (max) => {
    return Math.floor(Math.random() * Math.floor(max));
  };

  render() {
    return (
      <>
        <label
          className="rounded-label"
          htmlFor={`roundedInput-${this.props.id}`}
        ></label>
        <input
          type="text"
          id={`roundedInput-${this.props.id}`}
          className="rounded-input"
          value={this.props.value}
          onChange={this.props.onChange}
          disabled={this.props.disabled}
        />
      </>
    );
  }
}

export default RoundedInput;
