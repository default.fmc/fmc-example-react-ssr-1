import React, { Component } from "react";
import PropTypes from "prop-types";
import "./index.scss";
import {
  CircularProgressbarWithChildren,
  buildStyles,
} from "react-circular-progressbar";
import ChangingProgressProvider from "./ChangingProgressProvider";
//import "react-circular-progressbar/dist/styles.css";

export class SliderDot extends Component {
  static propTypes = {
    classes: PropTypes.arrayOf(PropTypes.string),
    number: PropTypes.number,
    timer: PropTypes.number,
    active: PropTypes.bool,
    onClick: PropTypes.func,
  };

  addDot = () => {
    if (this.props.active) {
      return (
        <div
          className={`slider-dot slider-dot_active ${
            this.props.classes ? this.props.classes.join(" ") : ""
          }`}
        >
          {this.props.timer ? (
            <ChangingProgressProvider values={[0, 100]}>
              {(percentage) => (
                <CircularProgressbarWithChildren
                  value={percentage}
                  strokeWidth={5}
                  background={true}
                  backgroundPadding={6}
                  styles={buildStyles({
                    pathTransition:
                      percentage === 0
                        ? "none"
                        : `stroke-dashoffset linear ${this.props.timer}ms`,
                    strokeLinecap: "round",
                    pathColor: "#4b9a1f",
                    trailColor: "transparent",
                    backgroundColor: "transparent",
                  })}
                >
                  <div className="slider-dot__text">
                    {this.props.number + 1}
                  </div>
                </CircularProgressbarWithChildren>
              )}
            </ChangingProgressProvider>
          ) : (
            <div className="slider-dot__text">{this.props.number + 1}</div>
          )}
        </div>
      );
    } else {
      return (
        <div
          onClick={() => this.props.onClick(this.props.number)}
          className="slider-dot"
        >
          <div className="slider-dot__inner-one">
            <div className="slider-dot__inner-two">
              <div className="slider-dot__text">{this.props.number + 1}</div>
            </div>
          </div>
        </div>
      );
    }
  };

  render() {
    return this.addDot();
  }
}

export default SliderDot;
