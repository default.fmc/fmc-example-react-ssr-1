import React from "react";
import SliderDot from ".";
import { action } from "@storybook/addon-actions";
import { withKnobs, boolean, number, text } from "@storybook/addon-knobs";

export default {
  component: SliderDot,
  title: "Atoms|SliderDot",
  parameters: {
    componentSubtitle: "Dot for switch slides",
  },
  decorators: [withKnobs],
};

export const Default = () => (
  <SliderDot
    active={boolean("Active", false)}
    timer={text("Timer duration, ms", 3000)}
    number={number("Slide Number", 2)}
    onClick={action("click-index")}
  />
);
