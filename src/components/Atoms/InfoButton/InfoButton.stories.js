import React from "react";
import InfoButton from ".";
import { action } from "@storybook/addon-actions";
import { withKnobs, text } from "@storybook/addon-knobs";

export default {
  title: "Atoms|InfoButton",
  component: InfoButton,
  decorators: [withKnobs],
};

export const Default = () => (
  <InfoButton onClick={action("click")}>
    {text("Button text", "Button")}
  </InfoButton>
);
