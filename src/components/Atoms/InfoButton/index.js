import React, { Component } from "react";
import PropTypes from "prop-types";
import "./index.scss";

export class InfoButton extends Component {
  static propTypes = {
    classes: PropTypes.arrayOf(PropTypes.string),
    onClick: PropTypes.func,
  };

  render() {
    return (
      <button
        className={`info-button ${
          this.props.classes ? this.props.classes.join(" ") : ""
        }`}
        onClick={this.props.onClick}
      >
        <div className="info-button__first-inner">
          <div className="info-button__second-inner">
            <div className="info-button__text">{this.props.children}</div>
          </div>
        </div>
      </button>
    );
  }
}

export default InfoButton;
