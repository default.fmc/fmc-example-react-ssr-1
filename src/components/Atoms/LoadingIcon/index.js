import React, { Component } from "react";
import PropTypes from "prop-types";
import "./index.scss";

export class LoadingIcon extends Component {
  static propTypes = {};

  render() {
    return (
      <div className="loading-icon">
        <svg
          className="progress-circle"
          width="120"
          height="120"
          viewBox="0 0 120 120"
        >
          <circle
            className="progress-circle__value"
            cx="60"
            cy="60"
            r="28"
            fill="none"
            stroke="#4B9A1F"
            strokeWidth="2px"
            strokeDasharray="175.84"
            strokeDashoffset="70.336"
          />
        </svg>
      </div>
    );
  }
}

export default LoadingIcon;
