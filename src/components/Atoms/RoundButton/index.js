import React, { Component } from "react";
import PropTypes from "prop-types";
import "./index.scss";

export class RoundButton extends Component {
  static propTypes = {
    onClick: PropTypes.func,
  };

  render() {
    return (
      <button className="round-button" onClick={this.props.onClick}>
        <div className="round-button__first-inner">
          <div className="round-button__second-inner">
            <div className="round-button__text">{this.props.children}</div>
          </div>
        </div>
      </button>
    );
  }
}

export default RoundButton;
