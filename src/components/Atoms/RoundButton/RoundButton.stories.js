import React from "react";
import RoundButton from ".";
import { action } from "@storybook/addon-actions";
import { withKnobs, text } from "@storybook/addon-knobs";

export default {
  title: "Atoms|RoundButton",
  component: RoundButton,
  decorators: [withKnobs],
};

export const Default = () => (
  <RoundButton onClick={action("click")}>
    {text("Button text", "+")}
  </RoundButton>
);
