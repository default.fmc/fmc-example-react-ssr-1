import React from "react";
import RoundPanel from ".";
import { withKnobs, text } from "@storybook/addon-knobs";

export default {
  title: "Atoms|RoundPanel",
  component: RoundPanel,
  decorators: [withKnobs],
};

export const Default = () => (
  <RoundPanel>{text("Panel text", "Text in panel")}</RoundPanel>
);
