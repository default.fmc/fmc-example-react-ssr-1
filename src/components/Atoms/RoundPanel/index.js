import React, { Component } from "react";
import PropTypes from "prop-types";
import "./index.scss";

export class RoundPanel extends Component {
  static propTypes = {
    classes: PropTypes.arrayOf(PropTypes.string),
  };

  render() {
    return (
      <div
        className={`round-panel ${
          this.props.classes ? this.props.classes.join(" ") : ""
        }`}
      >
        <div className="round-panel__first-inner">
          <div className="round-panel__second-inner">{this.props.children}</div>
        </div>
      </div>
    );
  }
}

export default RoundPanel;
