import React from "react";
import { action } from "@storybook/addon-actions";
import { withKnobs, text } from "@storybook/addon-knobs";
import Button from ".";
// import { Meta, Story, Preview } from "@storybook/addon-docs/blocks";

export default {
  title: "Atoms|Button",
  component: Button,
  decorators: [withKnobs],
};

export const Default = () => (
  <Button onClick={action("click")}>{text("Button text", "Button")}</Button>
);
