import React, { Component } from "react";
import PropTypes from "prop-types";
import "./index.scss";

export class Button extends Component {
  static propTypes = {
    classes: PropTypes.arrayOf(PropTypes.string),
    onClick: PropTypes.func,
  };

  render() {
    return (
      <button
        className={`button ${
          this.props.classes ? this.props.classes.join(" ") : ""
        }`}
        onClick={this.props.onClick}
      >
        <div className="button__first-inner">
          <div className="button__second-inner">
            <div className="button__text">{this.props.children}</div>
          </div>
        </div>
      </button>
    );
  }
}

export default Button;
