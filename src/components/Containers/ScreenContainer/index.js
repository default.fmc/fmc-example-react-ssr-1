import React, { Component } from "react";
import PropTypes from "prop-types";
import "./index.scss";

export class ScreenContainer extends Component {
  static propTypes = {
    classes: PropTypes.arrayOf(PropTypes.string),
    divider: PropTypes.bool,
  };

  render() {
    return (
      <section className="section">
        <div
          className={`screen-container ${
            this.props.classes ? this.props.classes.join(" ") : ""
          }`}
        >
          {this.props.children}
        </div>
        {this.props.divider && <div className="section__divider"></div>}
      </section>
    );
  }
}

export default ScreenContainer;
