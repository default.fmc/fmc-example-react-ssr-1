import React from "react";
import RoundedInput from "../../Atoms/RoundedInput";

import "./style.scss";

const CardProduct = (props) => {
  return (
    <div
      key={props.index}
      className={`product-card ${props.classes ? props.classes.join(" ") : ""}`}
    >
      <div className="product-card__first-panel">
        <div className="product-card__second-panel">
          <div className="product-card__img-container">
            <picture>
              <source srcSet={props.img.src[0]} />
              <img src={props.img.src[1]} alt={props.img.alt} />
            </picture>
          </div>
          <h3 className="product-card__headline">{props.model}</h3>
          <p className="product-card__text">{props.description}</p>
          <div className="product-card__total">
            <RoundedInput value={props.value} disabled={true} />
            <p className="product-card__price">{props.price} грн.</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CardProduct;
