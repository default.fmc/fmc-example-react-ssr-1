import React, { Component } from "react";
import SlickSlider from "react-slick";
/* import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css"; */
import PropTypes from "prop-types";
import MultiplePanel from "../MultiplePanel";
import "./index.scss";
import SliderDots from "../../Molecules/SliderDots";

export class SliderPanel extends Component {
  static propTypes = {
    classes: PropTypes.arrayOf(PropTypes.string),
    headerContent: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    sliderOptions: PropTypes.object.isRequired,
    enableDots: PropTypes.bool,
  };

  constructor(props) {
    super(props);

    this.state = {
      dots: [],
    };
  }

  componentDidMount() {
    const dots = [];
    for (let i = 0; i < this.slider.innerSlider.state.slideCount; i++) {
      dots.push({
        id: i + 1,
        active: i === 0 ? true : false,
      });
    }
    this.setState({ dots: dots });
  }

  changeSlide = (current, next) => {
    const newDots = this.state.dots.map((item) => {
      if (item.id === next + 1) {
        item.active = true;
      } else {
        item.active = false;
      }
      return item;
    });
    this.setState({ dots: newDots });
  };

  onClick = (number) => {
    this.slider.slickGoTo(number);
    this.changeSlide(this.slider.innerSlider.state.currentSlide, number);
  };

  render() {
    const defaultOptions = {
      beforeChange: (current, next) => this.changeSlide(current, next),
    };
    const allOptions = Object.assign(defaultOptions, this.props.sliderOptions);
    return (
      <div
        className={`slider-panel ${
          this.props.classes ? this.props.classes.join(" ") : ""
        }`}
      >
        <MultiplePanel
          classes={["slider-panel__multiple-panel"]}
          headerContent={this.props.headerContent}
          footerContent={
            this.props.enableDots && (
              <SliderDots
                onClick={this.onClick}
                dots={this.state.dots}
                timer={
                  allOptions.autoplay
                    ? allOptions.autoplaySpeed
                      ? allOptions.autoplaySpeed
                      : 3000
                    : null
                }
              />
            )
          }
        >
          <SlickSlider ref={(slider) => (this.slider = slider)} {...allOptions}>
            {this.props.children}
          </SlickSlider>
        </MultiplePanel>
      </div>
    );
  }
}

export default SliderPanel;
