import React from "react";
import SliderPanel from "./index";
import "../../Templates/HomeTemplate/index.scss";
import { withKnobs, text, boolean } from "@storybook/addon-knobs";

export default {
  title: "Organisms|SliderPanel",
  component: SliderPanel,
  decorators: [withKnobs],
};

const settings = {
  dots: false,
  arrows: false,
  infinite: true,
  adaptiveHeight: true,
  speed: 500,
  slidesToShow: 2,
  slidesToScroll: 1,
  pauseOnHover: false,
  autoplay: true,
  autoplaySpeed: 3000,
  responsive: [
    {
      breakpoint: 799,
      settings: {
        slidesToShow: 1,
      },
    },
  ],
};

export const Default = () => (
  <SliderPanel
    headerContent={text("Header content", "Text in header")}
    enableDots={boolean("Enable dots", true)}
    sliderOptions={settings}
  >
    <div className="slider-panel__item">
      <div className="slider-panel__content">
        <div className="slider-panel__inner">
          <div className="slider-panel__title">Испытания в России</div>
          <div className="slider-panel__text">
            Эффективность Нейтроника подтверждается результатами многочисленных
            испытаний, проведенных в ведущих лабораториях нашей страны.
          </div>
        </div>
      </div>
    </div>
    <div className="slider-panel__item">
      <div className="slider-panel__content">
        <div className="slider-panel__inner">
          <div className="slider-panel__title">Испытания в России</div>
          <div className="slider-panel__text">
            Эффективность Нейтроника подтверждается результатами многочисленных
            испытаний, проведенных в ведущих лабораториях нашей страны.
          </div>
        </div>
      </div>
    </div>
    <div className="slider-panel__item">
      <div className="slider-panel__content">
        <div className="slider-panel__inner">
          <div className="slider-panel__title">Испытания в России</div>
          <div className="slider-panel__text">
            Эффективность Нейтроника подтверждается результатами многочисленных
            испытаний, проведенных в ведущих лабораториях нашей страны.
          </div>
        </div>
      </div>
    </div>
    <div className="slider-panel__item">
      <div className="slider-panel__content">
        <div className="slider-panel__inner">
          <div className="slider-panel__title">Испытания в России</div>
          <div className="slider-panel__text">
            Эффективность Нейтроника подтверждается результатами многочисленных
            испытаний, проведенных в ведущих лабораториях нашей страны.
          </div>
        </div>
      </div>
    </div>
  </SliderPanel>
);
