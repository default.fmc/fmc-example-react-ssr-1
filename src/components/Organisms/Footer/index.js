import React, { Component } from "react";
import "./index.scss";

export class Footer extends Component {
  render() {
    return <footer className="footer">{this.props.children}</footer>;
  }
}

export default Footer;
