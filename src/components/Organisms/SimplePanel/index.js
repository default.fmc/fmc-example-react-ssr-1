import React, { Component } from "react";
import PropTypes from "prop-types";
import "./index.scss";

export class SimplePanel extends Component {
  static propTypes = {
    classes: PropTypes.arrayOf(PropTypes.string),
  };

  render() {
    return (
      <div
        className={`simple-panel ${
          this.props.classes ? this.props.classes.join(" ") : ""
        }`}
      >
        <div className="simple-panel__first-inner">
          <div className="simple-panel__second-inner">
            <div className="simple-panel__third-inner">
              {this.props.children}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SimplePanel;
