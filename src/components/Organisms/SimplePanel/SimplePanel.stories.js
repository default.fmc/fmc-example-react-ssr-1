import React from "react";
import SimplePanel from ".";
import { withKnobs, text } from "@storybook/addon-knobs";

export default {
  title: "Organisms|SimplePanel",
  component: SimplePanel,
  decorators: [withKnobs],
};

export const Default = () => (
  <SimplePanel>{text("Panel text", "Text in panel")}</SimplePanel>
);
