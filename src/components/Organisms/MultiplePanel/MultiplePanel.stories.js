import React from "react";
import MultiplePanel from ".";
import { withKnobs, text } from "@storybook/addon-knobs";

export default {
  title: "Organisms|MultiplePanel",
  component: MultiplePanel,
  decorators: [withKnobs],
};

export const Default = () => (
  <MultiplePanel
    headerContent={text("Header content", "Text in header")}
    footerContent={text("Footer content", "Text in footer")}
  >
    {text("Panel text", "Text in panel")}
  </MultiplePanel>
);
