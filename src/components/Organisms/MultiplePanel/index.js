import React, { Component } from "react";
import PropTypes from "prop-types";
import "./index.scss";

export class MultiplePanel extends Component {
  static propTypes = {
    classes: PropTypes.arrayOf(PropTypes.string),
    headerContent: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
    footerContent: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  };

  render() {
    return (
      <div
        className={`multiple-panel ${
          this.props.classes ? this.props.classes.join(" ") : ""
        }`}
      >
        {this.props.headerContent && (
          <div className="multiple-panel__header">
            {this.props.headerContent}
          </div>
        )}
        <div className="multiple-panel__first-inner">
          <div className="multiple-panel__second-inner">
            <div className="multiple-panel__third-inner">
              {this.props.children}
            </div>
          </div>
        </div>
        {this.props.footerContent && (
          <div className="multiple-panel__footer">
            {this.props.footerContent}
          </div>
        )}
      </div>
    );
  }
}

export default MultiplePanel;
