import React, { Component } from "react";
import PropTypes from "prop-types";
import SimplePanel from "../SimplePanel";
import Button from "../../Atoms/Button";
import "./index.scss";
import RoundPanel from "../../Atoms/RoundPanel";
import Icon from "../../Atoms/Icon";
import { home } from "../../../actions/home";

export class DecoratedPanel extends Component {
  static propTypes = {
    classes: PropTypes.arrayOf(PropTypes.string),
    iconUrl: PropTypes.string,
    buttonText: PropTypes.string,
    onClick: PropTypes.func,
  };

  render() {
    return (
      <div
        className={`decorated-panel ${
          this.props.classes ? this.props.classes.join(" ") : ""
        }`}
      >
        {this.props.iconUrl &&
          (this.props.mobileView || !this.props.isPlayingVideo) && (
            <div className="decorated-panel__label-wrap">
              <RoundPanel classes={["panel-label"]}>
                <Icon
                  url={this.props.iconUrl}
                  classes={["decorated-panel__icon"]}
                />
              </RoundPanel>
            </div>
          )}
        <SimplePanel>{this.props.children}</SimplePanel>
        {this.props.buttonText &&
          (this.props.mobileView || !this.props.isPlayingVideo) && (
            <div className="decorated-panel__button-wrap">
              <Button
                classes={["decorated-panel__button"]}
                onClick={this.props.onClick}
              >
                {this.props.buttonText}
              </Button>
            </div>
          )}
      </div>
    );
  }
}

export default DecoratedPanel;
