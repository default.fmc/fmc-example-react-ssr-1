import React from "react";
import DecoratedPanel from ".";
import { withKnobs, text } from "@storybook/addon-knobs";
import IconChip from "../../../../public/assets/images/icons/chip-icon.svg";

export default {
  title: "Organisms|DecoratedPanel",
  component: DecoratedPanel,
  decorators: [withKnobs],
};

export const Default = () => (
  <DecoratedPanel
    iconUrl={text("Image URL", IconChip)}
    buttonText={text("Button text", "BUTTON")}
  >
    {text("Panel text", "Text in panel")}
  </DecoratedPanel>
);
