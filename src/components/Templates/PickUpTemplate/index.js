import React from "react";
import "./index.scss";

const PickUpTemplate = (props) => {
  return <div className="pick-up">{props.children}</div>;
};

export default PickUpTemplate;
