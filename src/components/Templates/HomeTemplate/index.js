import React, { Component } from "react";
import "./index.scss";

export class HomeTemplate extends Component {
  render() {
    return <div className="container">{this.props.children}</div>;
  }
}

export default HomeTemplate;
