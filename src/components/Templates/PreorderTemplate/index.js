import React from "react";
import "./index.scss";

const PreorderTemplate = (props) => {
  return <div className="preorder">{props.children}</div>;
};

export default PreorderTemplate;
