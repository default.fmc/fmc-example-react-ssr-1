import React from "react";
import "./index.scss";

export class CartTemplate extends React.Component {
  render() {
    return <div className="container">{this.props.children}</div>;
  }
}

export default CartTemplate;
