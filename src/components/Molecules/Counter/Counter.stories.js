import React from "react";
import Counter from ".";
import { action } from "@storybook/addon-actions";
import { withKnobs, text } from "@storybook/addon-knobs";

export default {
  title: "Molecules|Counter",
  component: Counter,
  decorators: [withKnobs],
};

const getCount = (count) => {
  console.log("Count", count);
};

export const Default = () => <Counter returnQuantity={getCount} />;
