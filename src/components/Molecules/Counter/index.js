import React, { Component } from "react";
import PropTypes from "prop-types";
import RoundButton from "../../Atoms/RoundButton";
import RoundedInput from "../../Atoms/RoundedInput";
import "./index.scss";

export class Counter extends Component {
  static propTypes = {
    id: PropTypes.number,
    classes: PropTypes.arrayOf(PropTypes.string),
    returnQuantity: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.state = {
      value: 1,
    };
  }

  incrementValue = () => {
    this.setState((state, props) => {
      this.props.returnQuantity(+state.value + 1);
      return {
        value: +state.value + 1,
      };
    });
  };

  decrementValue = () => {
    if (this.state.value > 1) {
      this.setState((state, props) => {
        this.props.returnQuantity(+state.value - 1);
        return {
          value: +state.value - 1,
        };
      });
    }
  };

  render() {
    return (
      <div
        className={`counter ${
          this.props.classes ? this.props.classes.join(" ") : ""
        }`}
      >
        <RoundButton onClick={this.decrementValue}>—</RoundButton>
        <RoundedInput
          id={this.props.id}
          value={this.state.value}
          disabled={true}
        />
        <RoundButton onClick={this.incrementValue}>+</RoundButton>
      </div>
    );
  }
}

export default Counter;
