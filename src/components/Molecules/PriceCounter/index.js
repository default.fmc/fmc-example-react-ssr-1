import React, { Component } from "react";
import PropTypes from "prop-types";
import "./index.scss";
import functions from "../../../tools/functions";
import Counter from "../Counter";
import Button from "../../Atoms/Button";

export class PriceCounter extends Component {
  static propTypes = {
    id: PropTypes.number,
    classes: PropTypes.arrayOf(PropTypes.string),
    productName: PropTypes.string,
    productPrice: PropTypes.number,
    returnProduct: PropTypes.func,
    onClickButton: PropTypes.func,
  };

  constructor(props) {
    super(props);

    this.state = {
      name: props.productName,
      quantity: 1,
      price: {
        unitPrice: props.productPrice,
        salePrice: props.productPrice,
        simplePrice: props.productPrice,
      },
      sale: {
        percentValue: null,
        grnValue: null,
      },
    };
  }

  setQuantity = (quantity) => {
    this.setState({ quantity: quantity }, () => {
      this.setState(functions.countPrice(this.state));
    });
  };

  addToCart = () => {
    this.props.returnProduct(this.state, () => {
      this.setState(functions.countPrice(this.state));
    });
    this.props.onClickButton();
  };

  render() {
    return (
      <div
        className={`price-counter ${
          this.props.classes ? this.props.classes.join(" ") : ""
        }`}
      >
        <div className="price-counter__counter">
          <div className="price-counter__label price-counter__label-count">
            Количество:
          </div>
          <Counter id={this.props.id} returnQuantity={this.setQuantity} />
        </div>
        <div className="price-counter__button">
          {this.state.sale.grnValue > 0 && (
            <div className="price-counter__label price-counter__label-button">
              Вы экономите {this.state.sale.grnValue} грн.
            </div>
          )}
          <Button onClick={this.addToCart}>
            <span className="price-counter__button-icon"></span>В корзину:{" "}
            {this.state.price.salePrice} грн.
          </Button>
        </div>
      </div>
    );
  }
}

export default PriceCounter;
