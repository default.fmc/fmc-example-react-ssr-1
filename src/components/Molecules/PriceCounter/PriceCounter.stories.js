import React from "react";
import PriceCounter from ".";
import { action } from "@storybook/addon-actions";
import { withKnobs, boolean, number, text } from "@storybook/addon-knobs";

export default {
  component: PriceCounter,
  title: "Molecules|PriceCounter",
  decorators: [withKnobs],
};

export const Default = () => <PriceCounter />;
