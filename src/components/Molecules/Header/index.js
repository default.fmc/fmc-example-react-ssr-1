import React, { Component } from "react";
import Link from "next/link";
import PropTypes from "prop-types";
import "./index.scss";
import CartLink from "../../Atoms/CartLink";
import { Drawer } from "@material-ui/core";
import LogoSmallWebp from "../../../../public/assets/images/neitronik-logo-small.webp";
import LogoSmallPng from "../../../../public/assets/images/ios-support/neitronik-logo-small.png";

export class Header extends Component {
  static propTypes = {
    navItems: PropTypes.array,
    productsSum: PropTypes.number,
  };

  constructor(props) {
    super(props);

    this.state = {
      openSidebar: false,
    };
  }

  toggleDrawer = () =>
    this.setState((state) => ({ openSidebar: !state.openSidebar }));

  render() {
    return (
      <header className="header">
        <div className="hamburger" onClick={this.toggleDrawer}>
          <div className="hamburger__line"></div>
          <div className="hamburger__line"></div>
          <div className="hamburger__line"></div>
        </div>
        <Link href="/">
          <a className="header__logo">
            <picture>
              <source srcSet={LogoSmallWebp} />
              <img src={LogoSmallPng} alt="Logo" />
            </picture>
          </a>
        </Link>
        <div className="header__right-part">
          <nav className="header-nav">
            {this.props.navItems.map((item) => {
              return (
                <span
                  key={item.id}
                  onClick={item.onClick}
                  className="header-nav__link"
                >
                  {item.title}
                </span>
              );
            })}
          </nav>
          <div className="header__cart">
            <div className="header__phone">
              <a href="tel:+380967536542">+ 38 (096) 753 - 65 - 42</a>
            </div>
            <CartLink cartSumm={this.props.productsSum} />
          </div>
        </div>
        <Drawer
          anchor={"left"}
          open={this.state.openSidebar}
          onClose={this.toggleDrawer}
          classes={{ paperAnchorLeft: "sidebar" }}
        >
          <nav className="sidebar-content">
            {this.props.navItems.map((item) => {
              return (
                <div
                  key={item.id}
                  onClick={item.onClick}
                  className="sidebar-content__item"
                >
                  {item.title}
                </div>
              );
            })}
            <div className="sidebar-content__phone">
              <a href="tel:+380967536542">+ 38 (096) 753 - 65 - 42</a>
            </div>
          </nav>
        </Drawer>
      </header>
    );
  }
}

export default Header;
