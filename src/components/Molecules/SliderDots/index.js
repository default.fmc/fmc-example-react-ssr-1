import React, { Component } from "react";
import PropTypes from "prop-types";
import "./index.scss";
import SliderDot from "../../Atoms/SliderDot";

/**
 * Dots with timers and numbers for slider
 */

export class SliderDots extends Component {
  static propTypes = {
    classes: PropTypes.arrayOf(PropTypes.string),
    timer: PropTypes.number,
    dots: PropTypes.array,
    onClick: PropTypes.func,
  };

  render() {
    return (
      <div
        className={`slider-dots ${
          this.props.classes ? this.props.classes.join(" ") : ""
        }`}
      >
        {this.props.dots.map((item, index) => {
          return (
            <SliderDot
              key={item.id}
              active={item.active}
              timer={this.props.timer}
              number={index}
              onClick={this.props.onClick}
            />
          );
        })}
      </div>
    );
  }
}

export default SliderDots;
