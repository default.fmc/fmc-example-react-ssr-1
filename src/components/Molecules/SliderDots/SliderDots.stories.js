import React from "react";
import SliderDots from ".";
import { action } from "@storybook/addon-actions";
import { withKnobs, boolean, number, text } from "@storybook/addon-knobs";

export default {
  component: SliderDots,
  title: "Molecules|SliderDots",
  decorators: [withKnobs],
};

const dots = [
  {
    id: 1,
    active: true,
  },
  {
    id: 2,
    active: false,
  },
  {
    id: 3,
    active: false,
  },
  {
    id: 4,
    active: false,
  },
];

export const Default = () => (
  <SliderDots
    timer={text("Timer duration, ms", 3000)}
    dots={dots}
    onClick={action("click")}
  />
);
