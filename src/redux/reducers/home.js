import * as types from "../types";

const initialState = {
  dots: [],
};

export const home = (state = initialState, action) => {
  switch (action.type) {
    case types.EDIT_DOTS_DATA:
      return Object.assign({}, state, {
        dots: action.payload.dots,
      });

    default:
      return state;
  }
};
