const functions = {
  detectLanguage() {
    const parts = window.location.href.split("/");
    const chars = parts[3];
    const lang = chars.length === 2 ? chars : "ru";
    return {
      parts,
      chars,
      lang,
    };
  },
  countPrice(product) {
    const countedProduct = { ...product };
    countedProduct.price.simplePrice =
      countedProduct.quantity * countedProduct.price.unitPrice;
    if (countedProduct.quantity < 5) {
      countedProduct.price.salePrice =
        countedProduct.price.unitPrice * countedProduct.quantity * 1;
      countedProduct.sale.percentValue = 0;
      countedProduct.sale.grnValue = 0;
    }
    if (countedProduct.quantity >= 5 && countedProduct.quantity < 7) {
      countedProduct.sale.percentValue = Math.round((1 - 0.9) * 100);
    }
    if (countedProduct.quantity >= 7 && countedProduct.quantity < 10) {
      countedProduct.sale.percentValue = Math.round((1 - 0.86) * 100);
    }
    if (countedProduct.quantity >= 10) {
      countedProduct.sale.percentValue = Math.round((1 - 0.8) * 100);
    }
    if (countedProduct.quantity >= 5) {
      countedProduct.price.salePrice = Math.round(
        countedProduct.price.unitPrice *
          countedProduct.quantity *
          (1 - countedProduct.sale.percentValue / 100)
      );
      countedProduct.sale.grnValue =
        countedProduct.price.simplePrice - countedProduct.price.salePrice;
    }
    return countedProduct;
  },
  cartProductsSumm(products) {
    let summ = 0;
    products.forEach((element) => {
      summ += element.price.salePrice;
    });
    return summ;
  },
  grnSaleSumm(products) {
    let summ = 0;
    products.forEach((element) => {
      summ += element.sale.grnValue;
    });
    return summ;
  },
  deepMergeSum(obj1, obj2) {
    return Object.keys(obj1).reduce((acc, key) => {
      if (typeof obj2[key] === "object") {
        acc[key] = this.deepMergeSum(obj1[key], obj2[key]);
      } else if (obj2.hasOwnProperty(key) && !isNaN(parseFloat(obj2[key]))) {
        acc[key] = obj1[key] + obj2[key];
      }
      return acc;
    }, {});
  },
  pickUpNeitronik(gadgets) {
    const orders = [...gadgets];

    let universalNeitronik = { name: "Neitronik 5GRS", count: 0, price: 400 };
    let neitronikForMonitor = {
      name: "Neitronik MG-04M",
      count: 0,
      price: 364,
    };

    orders.forEach((item) => {
      if (
        item.name === "mobile" ||
        item.name === "router" ||
        item.name === "oven"
      ) {
        universalNeitronik.count += item.quantity;
      } else {
        neitronikForMonitor.count += item.quantity;
      }
    });
    return [universalNeitronik, neitronikForMonitor];
  },
};

export default functions;
