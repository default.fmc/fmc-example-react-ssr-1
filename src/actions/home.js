import store from "../redux/store";
import functions from "../tools/functions";
import { parseCookies, setCookie } from "nookies";
import { pickUpActions } from "./pickUp";

export const home = {
  editDotsData(array) {
    store.dispatch({
      type: "EDIT_DOTS_DATA",
      payload: {
        dots: array,
      },
    });
  },

  updateCartSumm() {
    const { CartProducts } = parseCookies();

    setCookie(
      null,
      "ProductsSumm",
      JSON.stringify(functions.cartProductsSumm(JSON.parse(CartProducts))),
      {
        path: "/",
      }
    );
    setCookie(
      null,
      "GrnSaleSumm",
      JSON.stringify(functions.grnSaleSumm(JSON.parse(CartProducts))),
      {
        path: "/",
      }
    );
  },

  updatePickUpSumm() {
    const { PickUpProducts } = parseCookies();

    setCookie(
      null,
      "PickUpSumm",
      JSON.stringify(functions.cartProductsSumm(JSON.parse(PickUpProducts))),
      {
        path: "/",
      }
    );
    setCookie(
      null,
      "PickUpGrnSaleSumm",
      JSON.stringify(functions.grnSaleSumm(JSON.parse(PickUpProducts))),
      {
        path: "/",
      }
    );
  },

  addToCart(product) {
    const { CartProducts } = parseCookies();
    let newProducts = [];
    if (CartProducts) {
      newProducts = [...JSON.parse(CartProducts)];
    }
    const newProduct = { ...product };
    if (!newProducts.find((item) => item.name === product.name)) {
      newProducts.push(product);
      setCookie(null, "CartProducts", JSON.stringify(newProducts), {
        path: "/",
      });
    } else {
      const changedProducts = newProducts.map((item) => {
        if (item.name === newProduct.name) {
          newProduct.quantity = item.quantity + newProduct.quantity;
          item = functions.countPrice(newProduct);
        }
        return item;
      });
      setCookie(null, "CartProducts", JSON.stringify(changedProducts), {
        path: "/",
      });
    }
    this.updateCartSumm();
  },

  addToPickUp(product) {
    const { PickUpProducts } = parseCookies();
    const newProduct = { ...product };
    let newProducts = [];
    if (PickUpProducts) {
      newProducts = [...JSON.parse(PickUpProducts)];
    }
    if (!newProducts.find((item) => item.name === product.name)) {
      newProducts.push(product);
      setCookie(null, "PickUpProducts", JSON.stringify(newProducts), {
        path: "/",
      });
    } else {
      let changedProducts = newProducts.map((item) => {
        if (item.name === newProduct.name) {
          newProduct.quantity = item.quantity + newProduct.quantity;
          item = functions.countPrice(newProduct);
        }
        return item;
      });
      setCookie(null, "PickUpProducts", JSON.stringify(changedProducts), {
        path: "/",
      });
    }
    this.updatePickUpSumm();
  },

  removeFromCart(productName) {
    const newProducts = [...JSON.parse(parseCookies().CartProducts)];
    newProducts.filter((item) => {
      if (item.name == productName) {
        if (item.quantity > 0) {
          item.quantity--;
          functions.countPrice(item);
        }
      }
    });
    setCookie(null, "CartProducts", JSON.stringify(newProducts), {
      path: "/",
    });
    this.updateCartSumm();
    return newProducts;
  },

  addFromCart(productName) {
    const newProducts = [...JSON.parse(parseCookies().CartProducts)];
    newProducts.filter((item) => {
      if (item.name == productName) {
        item.quantity++;
        functions.countPrice(item);
      }
    });
    setCookie(null, "CartProducts", JSON.stringify(newProducts), {
      path: "/",
    });
    this.updateCartSumm();
    return newProducts;
  },
};
